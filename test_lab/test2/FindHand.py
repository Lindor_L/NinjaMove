#! /usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import cv2

def findHand():
    pass

def main():
    pass

# Debug Code
cap = cv2.VideoCapture(0)
hand_cascade = cv2.CascadeClassifier('xmls/palm.xml')

while cap.isOpened():
    _,frame = cap.read()

    hands = hand_cascade.detectMultiScale(frame)
    print hands
    print '----------'

    good_hand_scale = None
    if () != hands:
        limit_max = np.max(hands[:, 2])
        if limit_max > 100:
            x,y,w,h = hands[hands[:,2] == limit_max][0]
            cv2.rectangle(frame, (x,y), (x+w, y+h), (255, 0, 0), 2)

    cv2.imshow('frame', frame)
    if 27 == cv2.waitKey(25)&0xFF:
        break

cv2.destroyAllWindows()
cap.release()
print 'End'
