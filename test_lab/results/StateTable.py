#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
1. 标记当前读取、写入位置；
2. 循环指令队列；
3. 读取、写入的Func，测试的Func；

'''
class StateTable:
    def __init__(self, rListLen = 16):
        self.wPoint = 0
        self.rPoint = 0
        self.rListLen = rListLen
        self.rdList = range(rListLen)
		# Debug Code
        #print rListLen
        for i in range(rListLen):
            self.rdList[i] = 0
    
    def readState(self):
        if self.wPoint == self.rPoint:
            return None
        else:
            value = self.rdList[self.rPoint]
            self.rPoint = (self.rPoint + 1)  % self.rListLen 
            return value

    def writeState(self, value):
        self.rdList[self.wPoint] = value
        test = (self.wPoint + 1) % self.rListLen
        if test != self.rPoint:
            self.wPoint = test

def main():
    # Debug
    stateTable = StateTable()

    for i in range(7):
        stateTable.writeState(i)
    for i in range(32):
        print i, ':', stateTable.readState()
    for i in range(15):
        stateTable.writeState(i)
    for i in range(32):
        print i, ':', stateTable.readState()
    for i in range(30):
        stateTable.writeState(i)
    for i in range(32):
        print i, ':', stateTable.readState()

if __name__ == '__main__':
    main()
