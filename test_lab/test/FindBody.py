#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
1. 检测人体位置；
2. 矫正距离与偏向；
3. 优化定位，提高速度；

'''
import numpy as np
import cv2
import math
import threading
# import time

global haar_xml
haar_xml = ['/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml',
            '/usr/share/opencv/haarcascades/haarcascade_frontalface_alt.xml',
            '/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml',
            '/usr/share/opencv/haarcascades/haarcascade_frontalface_alt_tree.xml']

# Weight Deal Code(TEST)
'''
def weightDeal(C, W, S):
    w = W / 3
    n = w / 0.6
    k = C - S * w 
    b = (n - w) / 2
    X = math.pi / n * (k + b) + math.pi / 2

    weight = int(100 * math.tan(X))
    weight ** 3
    print weight

    C2 = C + weight
    print C2
    if C2 < W / 2.5:
        # to left
        return 3
    # Debug Code
    elif C2 > W - W / 2.5:
        # to right
        return 4 
    else:
        return 0
'''

# Finds Thread Code(TEST)
'''
global bodyList
bodyList = [0,0,0]

def findThread(imgGray, scope, cascadeIndex, listIndex):
    global bodyList

    x,w = scope
    targetGray = imgGray[x:(x+w) % imgGray.shape[1], :]
    body_cascade = cv2.CascadeClassifier(haar_xml[cascadeIndex])
    bodys = body_cascade.detectMultiScale(targetGray)

    if () == bodys:
        bodyList[listIndex] = 0
        print 'FindThread:', listIndex, bodyList[listIndex]
        return
    else:
        limit = np.max(bodys[:,2])
        if imgGray.shape[1] / 6 > limit:
            bodyList[listIndex] = 0
            print 'FindThread:', listIndex, bodyList[listIndex]
            return
        bodyList[listIndex] = bodys[bodys[:,2] == limit][0]
        print 'FindThread:', listIndex, bodyList[listIndex]

'''

def findBody(imgGray, indexHaarXml = 0):
    global haar_xml
    body_cascade = cv2.CascadeClassifier(haar_xml[indexHaarXml])
    bodys = body_cascade.detectMultiScale(imgGray)

    # TEST Code:
    # threadList = []
    # w = imgGray.shape[1] / 5 * 3
    # scopeList = [(0, w), (w / 5 * 1, w), (w / 5 * 2, w)]

    # for i in range(3):
    #     threadList.append(threading.Thread(target=findThread, args=(imgGray, scopeList[i], indexHaarXml, i)))
    # for t in threadList:
    #     t.start()
    # for t in threadList:
    #     t.join()

    if () == bodys:
        return None
    else:
        limit = np.max(bodys[:,2])
        if imgGray.shape[1] / 6 > limit:
            return None
        return bodys[bodys[:,2] == limit][0]

def dealPosition(shape, scope):
    if None is scope:
        return -1
    else:
        sc_h, sc_w = shape[:2]
        sp_x, sp_y, sp_w, sp_h = scope
        # Debug Code:
        # print sc_w, sc_h
        # print scope

        if sp_h * 2 > sc_h:
            # stay away
            return 1
        elif sp_h * 3 < sc_h:
            # come over
            return 2 
        else:
            sp_center = sp_x + sp_w // 2
            # Debug Code
            if sp_center < sc_w / 2.5:
                # to left
                #return weightDeal(sp_center, sc_w, 0) 
                return 3
            # Debug Code
            elif sp_center > sc_w - sc_w / 2.5:
                # to right
                #return weightDeal(sp_center, sc_w, 2) 
                return 4 
            else:
                #return weightDeal(sp_center, sc_w, 1) 
                return 0

# Find in GameShell.py
'''
def getDealTime(haarIndex = 0):
    cap = cv2.VideoCapture(0)

    start = time.clock()
    if cap.isOpened():
        _, frame = cap.read()
        imgGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        scope = findBody(imgGray, haarIndex)
        if scope is not None:
            cap.release()
            dealPosition(imgGray.shape, scope)
        else:
            cap.release()
    return int((time.clock() - start) * 1000 * 3)
'''

def main():
    # Debug Main
    cap = cv2.VideoCapture(0)

    while True:
        _, frame = cap.read()
        imgGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Debug findBody
        scope = findBody(imgGray, 1)
        if scope is not None:
            x,y,w,h = scope
            cv2.rectangle(frame, (x,y), (x+w, y+h), (255,0,0), 1)

        # Debug setPosition
        tPos = dealPosition(imgGray.shape, scope)
        if -1 == tPos:
            print 'No Found'
        elif 1 == tPos:
            print 'stay away'
        elif 2 == tPos:
            print 'come over'
        elif 3 == tPos:
            print 'to left'
        elif 4 == tPos:
            print 'to right'
        else:
            print 'Nice !!!'

        cv2.imshow('frame', frame)
        if 27 == cv2.waitKey(25) & 0xFF:
            break
    cap.release()

if __name__ == '__main__':
    main()
