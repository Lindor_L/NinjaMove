### Welcome to the NinjaMove wiki!


**《NinjaMove》**

一款用脸去操作的PC游戏


1. 完全开源；
2. 融入人脸检测技术；
3. PyGame作为游戏引擎支持众多平台；

**安装方法：**

首先，安装支持库，pyopencv、numpy、pygame
如果在ubuntu下，可用以下命令
```
lindorl@UW:~$ sudo apt-get install python-numpy python-opencv python-pygame
```
最后，下载该游戏，解压并进入release目录，运行 Main.py 文件
```
lindorl@UW:~$ ./Main.py
```