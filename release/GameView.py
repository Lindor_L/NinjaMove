#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
根据StateTable的数据进行Game的编程；

'''

# Pygame Libs
import pygame
import sys
from pygame.locals import *
# Improve
import threading
import random

class GameView(threading.Thread):
    def __init__(self, stateTable, shellDelay, threadLock):
        threading.Thread.__init__(self)

        self.stateTable = stateTable
        self.shellDelay= shellDelay
        self.threadLock = threadLock
    
    def run(self):
        # Init Something
        pygame.init()
        
        SCREEN_SIZE = (640,480)
        screen = pygame.display.set_mode(SCREEN_SIZE,0,32)
        pygame.display.set_caption('Ninja Move ...')
        clock = pygame.time.Clock()
        TICK = 20
        CLOSE = False

        # Background
        backgroundColor = (255,255,255) 
        backImg = pygame.image.load('img/back.png').convert_alpha()

        # Back Music
        pygame.mixer.music.load('audio/FoolingMode.mp3')

        # Elves
        Ninja = pygame.image.load('img/naruto.png').convert_alpha()
        NinjaPosition = Ninja.get_rect()
        NinjaPosition.left, NinjaPosition.top = (640 - NinjaPosition.width) // 2, (480 - NinjaPosition.height - 10)
        NinjaNewPos = NinjaPosition

        crow1 = pygame.image.load('img/crow1.png').convert_alpha()
        crow2 = pygame.image.load('img/crow2.png').convert_alpha()
        crow = crow1
        crowPosition = crow.get_rect()
        crowPosition.left = -10
        delayN = self.shellDelay // 300
        delayT = 0
        crowStepSize = 20
        crowSpeed = (crowStepSize, 0)

        shit = pygame.image.load('img/shit.png').convert_alpha()
        shitPosition = shit.get_rect()
        shitPosition.left = -10
        shitPosition.top = crowPosition.height // 2 - shitPosition.height // 2
        shitNewPos = shitPosition
        # shitNewPos.top += crowPosition.height // 2
        shitStepSizeList = [[-10,  -5, 30, 35, 40, 45, 50, 55, 60],
                            [-10,  -5,  0, 10, 20, 25, 30, 40, 50],
                            [-20, -10,  0,  5, 10, 15, 20, 25, 50]]
        shitStepListNow = random.choice(shitStepSizeList)
        shitStepSize = random.choice(shitStepListNow)
        shitSpeed = (crowStepSize, shitStepSize)

        # Message
        font = pygame.font.Font(None, 108)
        lineHeight = font.get_linesize()
        msgStr = None
        
        while True:
            # Debug
            # print self.shellDelay
            for event in pygame.event.get():
                if event.type == QUIT:
                    CLOSE = True
            if CLOSE: break

            # Init vars

            # Get state
            # Get Lock
            self.threadLock.acquire()
            # Operation data
            state = self.stateTable.readState()
            # Release Lock
            self.threadLock.release()
            if -1 == state:
                msgStr = None
            elif 0 == state:
                msgStr = None
                NinjaNewPos = NinjaPosition
            elif 1 == state:
                msgStr = 'Stay away...!'
            elif 2 == state:
                msgStr = 'Come over...!'
            elif 3 == state:
                msgStr = None
                NinjaNewPos = NinjaNewPos.move((SCREEN_SIZE[0] - NinjaNewPos.right, 0))
            elif 4 == state:
                msgStr = None
                NinjaNewPos = NinjaNewPos.move((0 - NinjaNewPos.left, 0))

            # Crow and Shit Operations
            delayT = (delayT + 1) % delayN
            if 0 == delayT:
                crow = (crow1, crow2)[crow == crow1]
                crowPosition = crowPosition.move(crowSpeed)
                shitStepSize = random.choice(shitStepListNow)
                shitSpeed = (crowStepSize, shitStepSize)
                shitNewPos = shitNewPos.move(shitSpeed)
            if shitNewPos.right >= SCREEN_SIZE[0]:
                crowPosition.left = -10
                shitNewPos = shitPosition
                # shitNewPos.top = shitPosition.top + crowPosition.height // 2
                shitNewPos.left = crowPosition.left
                shitStepListNow = random.choice(shitStepSizeList)
            
            tSHN = shitNewPos.top - 10 > NinjaNewPos.top
            lSHN = shitNewPos.left - 30 > NinjaNewPos.left
            rSLN = shitNewPos.right + 15 < NinjaNewPos.right 
            bSLN = shitNewPos.bottom + 10 < NinjaNewPos.bottom
            if tSHN and lSHN and rSLN and bSLN:
                msgStr = 'Game Over !'
                CLOSE = True
            else:
                print 'No Over:'
            # Debug Code
            print tSHN, lSHN, rSLN, bSLN

            # Audio
            if not pygame.mixer.music.get_busy():
                pygame.mixer.music.play()

            # Video
            screen.fill(backgroundColor)
            screen.blit(backImg, (0,0))
            screen.blit(Ninja, NinjaNewPos)
            screen.blit(shit, shitNewPos)
            screen.blit(crow, crowPosition)
            screen.blit(font.render(msgStr, True, (70, 145, 37)), (lineHeight, lineHeight))
            pygame.display.flip()

            clock.tick(TICK)
        
def main():
    print None

if __name__ == '__main__':
    main()
