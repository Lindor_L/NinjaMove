#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
开启新线程从摄像头获取数据，检测人体，并将结果存入StateTable数据结构；

'''

# Game Shell
import numpy as np
import cv2
import FindBody
# Improve
import threading
# Get Time
# import time

class Camera2Table(threading.Thread):
    def __init__(self, stateTable, threadLock):
        threading.Thread.__init__(self)
        self.RUN = True
        self.stateTable = stateTable
        self.threadLock = threadLock

    def run(self):
        # Init someting
        cap = cv2.VideoCapture(0)
        IMG_SHAPE = None

        if cap.isOpened():
            IMG_SHAPE = cap.read()[1].shape
        else:
            return None

        # To operation capture in the while
        # Debug Code:
        testNum = 0
        while self.RUN and cap.isOpened():
            _, frame = cap.read()
            frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Get scope and state
            scope = FindBody.findBody(frameGray, 1)
            if scope is not None:
                state = FindBody.dealPosition(IMG_SHAPE, scope)
                # Get Lock
                self.threadLock.acquire()
                # Operation data
                self.stateTable.writeState(state)
                # Release Lock
                self.threadLock.release()
                # Debug Code:
                print 'TEST:', testNum, state
                x,y,w,h = scope
                xo = x + w // 2
                yo = y + h // 2
                print 'DISTANCE:', xo, yo
                testNum += 1
            else:
                continue
            
        # Release someting
        cap.release()    

    def close(self):
        self.RUN = False

def getDealTime(haarIndex = 0):
    cap = cv2.VideoCapture(0)

    #start = time.clock()
    start = cv2.getTickCount()
    if cap.isOpened():
        _, frame = cap.read()
        imgGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        scope = FindBody.findBody(imgGray, haarIndex)
        if scope is not None:
            cap.release()
            FindBody.dealPosition(imgGray.shape, scope)
        else:
            cap.release()
    #return int((time.clock() - start) * 1000 * 3)
    return int((cv2.getTickCount() - start) / 1000000 * 3)

def main():
    print getDealTime()

if __name__ == '__main__':
    main()
