#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
1. 开启新线程从摄像头获取数据，检测人体，并将结果存入StateTable数据结构；
2. 根据StateTable的数据进行Game的编程；

'''

# Game Shell
import GameShell
# Game View
import GameView
# Data Struct
import StateTable
# Improve
import threading
# Init Someting
# import FindBody

def main():
    # Init:
    stateTable = StateTable.StateTable(32)
    threadLock = threading.Lock()
    shellDelay = GameShell.getDealTime(0)
    print shellDelay

    # Run:
    camera2Table = GameShell.Camera2Table(stateTable, threadLock)
    gameView = GameView.GameView(stateTable, shellDelay, threadLock)
    camera2Table.start()
    gameView.start()

    # Close:
    gameView.join()
    camera2Table.close()
    print 'End of Ninja Move'

if __name__ == '__main__':
    main()
